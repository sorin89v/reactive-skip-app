package com.demo.skiapp.scenarios

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder

object GetPlaces {

  val getPlacesHttp: HttpRequestBuilder = http("Get Places")
    .get("/v1/places")
    .check(status is 200)


  val getPlaces: ScenarioBuilder = scenario("Get All Places")
    .repeat(1) {
      exec(getPlacesHttp)
    }

}
