package com.demo.skiapp.simulation

import com.demo.skiapp.util._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._
import com.demo.skiapp.scenarios.GetPlaces
import io.gatling.http.protocol.HttpProtocolBuilder

class PlaceMicroserviceSimulation extends Simulation {

  val httpConf: HttpProtocolBuilder = http.baseUrl(Environment.baseURL)
    .headers(Headers.commonHeader)

  val placeMicroserviceScenarios = List(

    GetPlaces.getPlaces.inject(
      nothingFor(5 seconds),
      atOnceUsers(Environment.users.toInt),
      rampUsersPerSec(5) to 100 during (30 seconds)
    )

  )

  setUp(placeMicroserviceScenarios)
    .protocols(httpConf)
    .maxDuration(3 minutes)
    .assertions(
      global.responseTime.max.lte(Environment.maxResponseTime.toInt)
    )
}
