package com.demo.skiapp.util

object Environment {
  val baseURL: String = scala.util.Properties.envOrElse("baseURL", "http://localhost:11099")
  val users: String = scala.util.Properties.envOrElse("numberOfUsers", "300")
  val maxResponseTime: String = scala.util.Properties.envOrElse("maxResponseTime", "20000") // in milliseconds
}
