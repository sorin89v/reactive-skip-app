package com.demo.skiapp.hazelcast;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private final CacheClient cacheClient;



    @Autowired
    public ApplicationStartup(final CacheClient cacheClient) {
        this.cacheClient = cacheClient;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {

        final Config hazelcastConfig = new Config();
        final JoinConfig config =  hazelcastConfig.getNetworkConfig().getJoin();
        System.out.println(config);

       final Car toUpdate = Car.builder()
               .name("sd")
               .number("1234")
               .build();

        cacheClient.put("abc", toUpdate);

        final Car retrieved = cacheClient.get("abc");
        System.out.println(retrieved);
    }
}
