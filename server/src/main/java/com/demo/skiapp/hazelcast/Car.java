package com.demo.skiapp.hazelcast;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

//@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Car {
    private String number;
    private String name;
}
