package com.demo.skiapp.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "method", visible = true)
@JsonSubTypes(value = {
        @JsonSubTypes.Type(name = "OPEN_URL", value = InteractiveIntentOpenUrlDTO.class),
        @JsonSubTypes.Type(name = "DOWNLOAD_COUPON", value = InteractiveIntentCouponDTO.class),
})
public abstract class InteractiveIntentDTO<T> {

    @NotNull(message = "{constraint.interactive.intent.method.null}")
    private InteractiveIntentTypeDTO method;

    @NotNull(message = "{constraint.interactive.intent.method.null}")
    public abstract InteractiveIntentTypeDTO getMethod();

    @NotNull(message = "{constraint.interactive.intent.params.null}")
    public abstract T getParams();
}

