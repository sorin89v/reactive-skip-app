package com.demo.skiapp.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InteractiveIntentCouponDTO extends InteractiveIntentDTO<InteractiveCouponParamsDTO> {

    @Valid
    @NotNull(message = "{constraint.interactive.intent.params.null}")
    private InteractiveCouponParamsDTO params;

    @Override
    public InteractiveIntentTypeDTO getMethod() {
        return InteractiveIntentTypeDTO.DOWNLOAD_COUPON;
    }

    @Override
    public InteractiveCouponParamsDTO getParams() {
        return this.params;
    }
}
