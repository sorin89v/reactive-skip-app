package com.demo.skiapp.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class InteractiveCreativeActionDTO {

    @NotNull(message = "{constraint.interactive.creative.action.intent.null}")
    @Valid
    private InteractiveIntentDTO<?> intent;
}
