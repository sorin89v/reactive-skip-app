package com.demo.skiapp.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InteractiveIntentOpenUrlDTO extends InteractiveIntentDTO<InteractiveOpenUrlParamsDTO> {

    @Valid
    @NotNull(message = "{constraint.interactive.intent.params.null}")
    private InteractiveOpenUrlParamsDTO params;

    @Override
    public InteractiveIntentTypeDTO getMethod() {
        return InteractiveIntentTypeDTO.OPEN_URL;
    }

    @Override
    public InteractiveOpenUrlParamsDTO getParams() {
        return this.params;
    }
}
