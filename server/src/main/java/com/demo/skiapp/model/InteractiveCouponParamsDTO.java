package com.demo.skiapp.model;

import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InteractiveCouponParamsDTO {

    @NotEmpty(message = "{constraint.interactive.intent.coupon.url.empty}")
    private String couponUrl;

    @NotEmpty(message = "{constraint.interactive.intent.coupon.url.fallback.empty}")
    private String fallbackUrl;
}

