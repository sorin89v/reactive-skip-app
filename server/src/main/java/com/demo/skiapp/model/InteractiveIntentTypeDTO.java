package com.demo.skiapp.model;

public enum InteractiveIntentTypeDTO {

    OPEN_URL,
    DOWNLOAD_COUPON
}
