package com.demo.skiapp.model;

import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@NoArgsConstructor
@Data
public class InteractiveOpenUrlParamsDTO {

    @NotEmpty(message = "{constraint.interactive.intent.url.empty}")
    private String url;
}
