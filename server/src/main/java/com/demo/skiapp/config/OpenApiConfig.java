package com.demo.skiapp.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.parser.OpenAPIV3Parser;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class OpenApiConfig {

    private static final String VERSION_PATTERN = "/v[0-9].*/";
    private static final String SECURITY_SCHEME = "basicScheme";
    private static final String API_NAME = "Ski API";
    private static final Pattern versionedPathPattern = Pattern.compile(VERSION_PATTERN);
    private static final SecurityScheme securityScheme = new SecurityScheme()
            .type(SecurityScheme.Type.HTTP)
            .scheme("basic");

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .components(new Components().addSecuritySchemes(SECURITY_SCHEME, securityScheme))
                .info(new Info().title(API_NAME));
    }

    @Bean
    public GroupedOpenApi version1() {
        final String[] paths = {"/v1/**"};
        return GroupedOpenApi.builder().setGroup("v1").pathsToMatch(paths)
                .addOpenApiCustomiser(openApi -> new OpenAPI()
                        .components(new Components().addSecuritySchemes(SECURITY_SCHEME, securityScheme))
                        .info(new Info().title(API_NAME).version("v1")))
                .build();
    }

    @Bean
    public GroupedOpenApi version2() {
        final String[] paths = {"/v2/**"};

        return GroupedOpenApi.builder().setGroup("v2").pathsToMatch(paths)
                .addOpenApiCustomiser(openApi -> new OpenAPI()
                        .components(new Components().addSecuritySchemes(SECURITY_SCHEME, securityScheme))
                        .info(new Info().title(API_NAME).version("v2")))
                .build();
    }

    @Bean
    public OpenApiCustomiser customiserLatestAPI() {
        final OpenAPI api = new OpenAPIV3Parser().read("spec/ski.yaml");
        final OpenApiCustomiser custom = openApi -> {
            openApi.setComponents(api.getComponents());
            openApi.setPaths(latestPaths(api.getPaths()));
            openApi.setExtensions(api.getExtensions());
            openApi.setInfo(api.getInfo());
            openApi.setExternalDocs(api.getExternalDocs());
            openApi.setOpenapi(api.getOpenapi());
            openApi.setSecurity(api.getSecurity());
            openApi.setServers(api.getServers());
            openApi.setTags(api.getTags());
        };
        custom.customise(api);
        return custom;
    }

    private Paths latestPaths(final Paths allPaths) {
        final Map<String, List<VersionedPath>> groupedByPath = allPaths.entrySet()
                .stream()
                .map(entry -> {
                    final String fullPath = entry.getKey();
                    final String path = fullPath.replaceAll(VERSION_PATTERN, "");
                    final Matcher matcher = versionedPathPattern.matcher(fullPath);
                    if (matcher.find()) {
                        return VersionedPath.builder()
                                .path(path)
                                .pathItem(entry.getValue())
                                .version(matcher.group())
                                .build();
                    }
                    throw new IllegalArgumentException(String.format("Path: '%s' must contain version!", path));
                })
                .collect(Collectors.groupingBy(VersionedPath::getPath, Collectors.toList()));

        final Map<String, PathItem> latestPaths = groupedByPath.values()
                .stream()
                .map(versionedPaths -> {
                            final VersionedPath latest = versionedPaths.stream()
                                    .min((v1, v2) -> v2.getVersion().compareTo(v1.getVersion()))
                                    .orElseThrow(() -> new IllegalArgumentException("Empty versioned path list!"));

                            return new AbstractMap.SimpleEntry<>(latest.getVersionedPath(), latest.getPathItem());
                        }
                ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        final Paths latest = new Paths();
        latest.putAll(latestPaths);
        return latest;
    }

    @Data
    @Builder
    @AllArgsConstructor
    private static class VersionedPath {
        private String version;
        private String path;
        private PathItem pathItem;

        public String getVersionedPath() {
            return version + path;
        }
    }
}
