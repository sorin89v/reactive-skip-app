package com.demo.skiapp.config;

import org.hibernate.dialect.MariaDB102Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;


public class ExtendedMariaDbDialect extends MariaDB102Dialect {
//public class ExtendedMariaDbDialect extends MariaDBDialect {

    public ExtendedMariaDbDialect() {
        super();

        registerFunction(
                "JSON_SEARCH",
                new StandardSQLFunction(
                        "JSON_SEARCH",
                        StandardBasicTypes.STRING
                )
        );

        registerFunction(
                "REGEX",
                new StandardSQLFunction(
                        "REGEX",
                        StandardBasicTypes.STRING
                )
        );
    }
}
