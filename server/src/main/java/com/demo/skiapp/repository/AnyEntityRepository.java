package com.demo.skiapp.repository;

import com.demo.skiapp.entity.AnyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnyEntityRepository extends JpaRepository<AnyEntity, String> {
}
