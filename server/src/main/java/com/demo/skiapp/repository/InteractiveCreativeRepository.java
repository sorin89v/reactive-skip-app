package com.demo.skiapp.repository;

import com.demo.skiapp.entity.interactive.InteractiveCreativeDetail;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InteractiveCreativeRepository extends JpaRepository<InteractiveCreativeDetail, Integer> {



    @Query("SELECT ia FROM InteractiveCreativeDetail ia"
            + " JOIN FETCH ia.emitters emitters "
            + " WHERE ia.creative.id = :creativeId ")
    Optional<InteractiveCreativeDetail> findActionFetchEmittersByCreativeParent(@Param("creativeId") final Integer creativeId);

    @Query("SELECT ia FROM InteractiveCreativeDetail ia"
            + " JOIN FETCH ia.actionIntents actionIntents"
            + " JOIN FETCH actionIntents.intent intent "
            + " WHERE ia.creative.id = :creativeId ")
    Optional<InteractiveCreativeDetail> findActionFetchIntentsByCreativeParent(@Param("creativeId") final Integer creativeId);


    @Query(value = "DELETE FROM InteractiveCreativeDetail ia WHERE ia.creative.id = :creativeId")
    @Modifying
    void deleteAllByCreativeId(@Param("creativeId") final Integer creativeId);

}
