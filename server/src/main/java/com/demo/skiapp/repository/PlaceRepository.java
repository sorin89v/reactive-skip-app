package com.demo.skiapp.repository;

import com.demo.skiapp.entity.PlaceEntity;
import com.demo.skiapp.entity.model.PlaceZoneEntity;
import java.util.List;
import java.util.Optional;
import javax.persistence.QueryHint;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

public interface PlaceRepository extends JpaRepository<PlaceEntity, Integer>, JpaSpecificationExecutor<PlaceEntity> {

    @Query("SELECT p FROM PlaceEntity p WHERE p.placeName = :placeName")
//    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true")})
    Optional<PlaceEntity> findByName(final String placeName);

    @Query("SELECT NEW com.demo.skiapp.entity.model.PlaceZoneEntity(p.placeName, p.zone) FROM PlaceEntity p")
    List<PlaceZoneEntity> findCustom(Pageable pageable);



    @Query("SELECT p FROM PlaceEntity p WHERE JSON_SEARCH(p.customAttributes, 'one', :attributeValue) IS NOT NULL")
    List<PlaceEntity> findCustomByAttributeFunctionSearch(final String attributeValue);


    @Query(value = "SELECT * FROM places WHERE attributes REGEXP :value", nativeQuery =  true)
    List<PlaceEntity> findAllByCustomAttributesMatchesRegex(@Param("value") final String attributeValue);

    @Query(value = "SELECT * FROM places p WHERE p.attributes like %:value%", nativeQuery = true)
    List<PlaceEntity> findCustomByAttributeLike(@Param("value") final String attributeValue);


    //Match agains

    @Query(value = "SELECT * FROM places WHERE MATCH(attributes) AGAINST (:value) > 0 AND place_Id > 100", nativeQuery = true)
    List<PlaceEntity> findCustomByAttributeMatchAndAnotherIndex(@Param("value") final String attributeValue);

    @Query(value = "SELECT * FROM places WHERE MATCH(attributes) AGAINST (:value) > 0", nativeQuery = true)
    List<PlaceEntity> findCustomByAttributeMatch(@Param("value") final String attributeValue);

    @Query(value = "SELECT * FROM places WHERE MATCH(attributes) AGAINST (:value) > 0 AND place_name = 'abc'", nativeQuery = true)
    List<PlaceEntity> findCustomByAttributeMatchAndAnotherIndex2(@Param("value") final String attributeValue);

    @Query("SELECT p.placeId FROM PlaceEntity p WHERE p.placeName LIKE CONCAT(CONCAT('%', :placeName),'%')ORDER BY p.placeId ASC")
    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value ="true")})
            List<Integer> findComplex(final String placeName, final Pageable pageable);
}
