package com.demo.skiapp.repository;

import com.demo.skiapp.entity.interactive.InteractionActionIntentPk;
import com.demo.skiapp.entity.interactive.InteractiveActionIntent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InteractiveActionIntentRepository extends JpaRepository<InteractiveActionIntent, InteractionActionIntentPk> {


}
