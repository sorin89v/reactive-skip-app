package com.demo.skiapp.repository;

import com.demo.skiapp.entity.interactive.Creative;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreativeRepository extends JpaRepository<Creative, Integer> {
}
