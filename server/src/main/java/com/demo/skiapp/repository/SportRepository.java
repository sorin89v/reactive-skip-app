package com.demo.skiapp.repository;

import com.demo.skiapp.entity.SportEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SportRepository extends JpaRepository<SportEntity, String> {

    @Query("SELECT sport FROM SportEntity sport WHERE sport.sportName in (:sportNames)")
    List<SportEntity> findInSearch(@Param("sportNames") List<String> sportNames);
}
