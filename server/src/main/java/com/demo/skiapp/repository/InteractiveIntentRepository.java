package com.demo.skiapp.repository;

import com.demo.skiapp.entity.interactive.InteractiveIntent;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InteractiveIntentRepository extends JpaRepository<InteractiveIntent, Integer> {


    @Query("DELETE FROM InteractiveIntent ii WHERE ii.id IN :intentIds")
    @Modifying
    void deleteAllByIntentId(final @Param("intentIds") Set<Integer> entityIds);

}
