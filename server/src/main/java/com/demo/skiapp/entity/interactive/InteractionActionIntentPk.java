package com.demo.skiapp.entity.interactive;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InteractionActionIntentPk implements Serializable {

    private static final long serialVersionUID = -55355040620815790L;

    @Column(name = "creative_action_id", nullable = false)
    private Integer actionId;

    @Column(name = "creative_intent_id", nullable = false)
    private Integer intentId;

    @Column(name = "intent_action_trigger", nullable = false)
    private String trigger;
}

