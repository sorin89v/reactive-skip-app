package com.demo.skiapp.entity.model;

import lombok.Data;

@Data
public class PlaceZoneEntity {

    public PlaceZoneEntity(final String placeName, final Integer zone){
        this.placeName = placeName;
        this.zone = zone;
    }

    private String placeName;
    private Integer zone;
}
