package com.demo.skiapp.entity.interactive;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "creative_intents")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InteractiveIntent {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "method", nullable = false)
    private String method;

    @Column(name = "params", nullable = false)
    private String params;


//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "creative_actions_intents",
//            joinColumns = {@JoinColumn(name = "creative_action_id", referencedColumnName = "id", nullable = false)},
//            inverseJoinColumns = {@JoinColumn(name = "id", referencedColumnName = "creative_action_id")}
//    )

    //WORKING

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "intent")
    private List<InteractiveActionIntent> actionIntents = new ArrayList<>();

//    public void addActionIntent(final InteractiveActionIntent actionIntent){
//        this.actions.add(actionIntent);
//        actionIntent.setIntent(this);
//    }

//    public void remove(final InteractiveActionIntent actionIntent){
//        actionIntent.setIntent(null);
//        actionIntents.remove(actionIntent);
//    }
}
