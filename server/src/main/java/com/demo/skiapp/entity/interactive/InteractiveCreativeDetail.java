package com.demo.skiapp.entity.interactive;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "creative_actions")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InteractiveCreativeDetail {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "message", nullable = false)
    private String message;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "creative_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Creative creative;

//    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "creativeDetail")
    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "creative_action_id", referencedColumnName = "id", insertable = false, updatable = false)
    private List<InteractiveEmitter> emitters = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "action")
    private List<InteractiveActionIntent> actionIntents = new ArrayList<>();

    public void addActionIntent(final InteractiveActionIntent actionIntent) {
        this.actionIntents.add(actionIntent);
        actionIntent.setAction(this);
    }
}
