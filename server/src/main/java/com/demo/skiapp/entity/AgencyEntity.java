package com.demo.skiapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "agency")
public class AgencyEntity {

    @Id
    @Column(name = "agency_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer agencyid;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "place_agency_id", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "ad"))
    @JsonIgnore
    private PlaceEntity place;

    @Column(name = "agency_name")
    private String name;
}
