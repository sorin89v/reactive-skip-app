package com.demo.skiapp.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Table(name = "sports")
@ToString(exclude = "parentPlace")
@EqualsAndHashCode(exclude = "parentPlace")
@IdClass(SportEntityPrimaryKey.class)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SportEntity implements Serializable {

    @Id
    @Column(name = "sport_name")
    private String sportName;

    @Column(name = "daily_average_cost")
    private BigDecimal dailyCostAverage;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "place_parent", foreignKey = @ForeignKey(name = "any_foreign_key"))
    @JsonManagedReference
    private PlaceEntity parentPlace;
}
