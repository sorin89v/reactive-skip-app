package com.demo.skiapp.entity;

import com.demo.skiapp.entity.converter.HashMapConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

@Getter
@Setter
@EqualsAndHashCode(exclude = "agency")
@Entity
@Table(name = "places")
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})

//@javax.persistence.Cacheable
public class PlaceEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "place_id")
    private Integer placeId;

    @Column(name = "place_name", unique = true)
    private String placeName;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "parentPlace")
    @JsonBackReference
    List<SportEntity> sports = new ArrayList<>();

    @Column(name = "country")
    private String country;

    @Column(name = "zone")
    private Integer zone;

    @Column(name = "free_rooms")
    private Integer freeRooms;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "place", optional = false)
    private AgencyEntity agency;

//    @Type(type = "json")
//    @Column(name = "attributes", columnDefinition = "json")
//    private String customAttributes;

    @Convert(converter = HashMapConverter.class)
    @Column(name = "attributes", columnDefinition = "longtext")
    private Map<String, Object> customAttributes;

//    @Version
//    private Long version;

    public void addSport(SportEntity sport) {
        this.sports.add(sport);
        sport.setParentPlace(this);
    }
}
