package com.demo.skiapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.Data;

@Data
@Entity
@Table(name = "entity")
public class AnyEntity {

    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "field")
    private String field;

    @Version
    private Long version;
}
