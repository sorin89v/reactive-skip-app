package com.demo.skiapp.mapper;

import com.demo.skiapp.entity.AnyEntity;
import com.demo.skiapp.entity.PlaceEntity;
import com.demo.skiapp.entity.SportEntity;
import com.demo.skiapp.entity.model.PlaceZoneEntity;
import com.demo.skiapp.model.AddPlace;
import com.demo.skiapp.model.Entity;
import com.demo.skiapp.model.Place;
import com.demo.skiapp.model.PlaceZone;
import com.demo.skiapp.model.Sport;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CustomDataMapper {

    PlaceEntity toPlaceEntity(Place model);

    Place toPlaceModel(PlaceEntity entity);

    PlaceEntity toPlaceEntity(AddPlace model);

    SportEntity toSportEntity(Sport model);

    Sport toSportModel(SportEntity entity);

    PlaceZone toPlaceZone(PlaceZoneEntity entity);

    AnyEntity toEntityEntity(Entity entity);

    Entity toEntityModel(AnyEntity entity);
}
