package com.demo.skiapp.controller;

import com.demo.skiapp.api.GenericEntityApiDelegate;
import com.demo.skiapp.model.Entity;
import com.demo.skiapp.service.EntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Controller
@Slf4j
public class EntityController implements GenericEntityApiDelegate {

    private final EntityService entityService;

    @Autowired
    public EntityController(EntityService entityService) {
        this.entityService = entityService;
    }

    @Override
    public Mono<ResponseEntity<Entity>> addEntity(
            final Entity entity) {
        return Mono.fromCallable(() -> entityService.addEntity(entity))
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }
}
