package com.demo.skiapp.controller;

import com.demo.skiapp.api.PlacesApiDelegate;
import com.demo.skiapp.entity.interactive.Creative;
import com.demo.skiapp.entity.interactive.InteractiveCreativeDetail;
import com.demo.skiapp.model.AddPlace;
import com.demo.skiapp.model.BookingInfo;
import com.demo.skiapp.model.Place;
import com.demo.skiapp.model.PlaceZone;
import com.demo.skiapp.model.UpdatablePlaceFields;
import com.demo.skiapp.service.InteractiveService;
import com.demo.skiapp.service.PlaceService;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Controller
@Slf4j
public class PlaceController implements PlacesApiDelegate {

    private final PlaceService placeService;
    private final InteractiveService interactiveService;

    @Autowired
    public PlaceController(
            PlaceService placeService,
            InteractiveService interactiveService) {
        this.placeService = placeService;
        this.interactiveService = interactiveService;
    }

    @Override
    public Mono<ResponseEntity<Place>> bookPlace(final Integer placeId, final BookingInfo bookingInfo) {
        return Mono.fromCallable(() -> placeService.bookPlace(placeId, bookingInfo))
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }

    @Override
    public Mono<ResponseEntity<Place>> addPlace(AddPlace newPlace) {
        return Mono.fromCallable(() -> placeService.addNewPlace(newPlace))
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }

    @Override
    public Mono<ResponseEntity<List<PlaceZone>>> getPlaceZones(final Integer page, final Integer limit) {
        return Mono.fromCallable(() -> placeService.placeZones(page, limit))
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }

    @Override
    public Mono<ResponseEntity<Place>> deletePlace(Integer placeId) {
        return Mono.fromCallable(() -> placeService.deletePlace(placeId))
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }

    @Override
    public Mono<ResponseEntity<Place>> getPlace(Integer placeId) {
        return Mono.fromCallable(() -> placeService.getPlace(placeId))
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }

    @Override
    public Mono<ResponseEntity<List<Place>>> getPlaces(String freeSearchPattern, Integer page, Integer limit) {
        return Mono.fromCallable(() -> placeService.getPlaces(freeSearchPattern, page, limit))
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }

    @Override
    public Mono<ResponseEntity<List<Place>>> getPlacesByAttributes(final String attributeValue) {
        return Mono.fromCallable(() -> placeService.getPlacesByAttributes(attributeValue))
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }

    @Override
    public Mono<ResponseEntity<Place>> updatePlace(Integer placeId, UpdatablePlaceFields updatablePlaceFields) {
        return Mono.fromCallable(() -> placeService.update(placeId, updatablePlaceFields))
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }

    @Override
    public Mono<ResponseEntity<List<Place>>> getV2Places() {
        //        return Mono.fromCallable(placeService::getAllPlaces)
        //                .map(ResponseEntity::ok)
        //                .subscribeOn(Schedulers.elastic());

        //        interactiveService.saveSomething();

        final Creative creative = interactiveService.saveCreative();
        final InteractiveCreativeDetail action = interactiveService.saveSomething(creative);
        System.out.println("No transaction");

        final InteractiveCreativeDetail retrieved = interactiveService.getCreative(action.getId());

//        System.out.println(retrieved.getEmitters().size());

        retrieved.getEmitters().forEach(
                interactiveEmitter -> {
                    System.out.println(interactiveEmitter);
                    System.out.println("finished emitter");
                }

        );

        retrieved.getActionIntents().forEach(
                actionIntent -> {
                    System.out.println("ActionIntent: " + actionIntent);
                    System.out.println("Intent" + actionIntent.getIntent());
                    System.out.println("finished intents");
                }

        );


        // Delete repository

//        interactiveService.deleteCreative(creative);

        interactiveService.updateCreative(creative);




        List<Place> response = new ArrayList<>();
        return Mono.fromCallable(() -> response)
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }
}
