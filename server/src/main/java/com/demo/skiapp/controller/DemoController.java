package com.demo.skiapp.controller;

import com.demo.skiapp.model.InteractiveCreativeActionDTO;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@RestController
public class DemoController {

    @GetMapping(value = "/test")
    public Mono<ResponseEntity<String>> testInheritance(
            @Valid @RequestBody final InteractiveCreativeActionDTO creativeActionDTO) {
        System.out.println(creativeActionDTO);
        return Mono.just("Ok")
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }

    @GetMapping(value = "/test2/{id}")
    public Mono<ResponseEntity<String>> test2(
            @NotNull @PathVariable(value = "id") final int id) {
        System.out.println(id);
        return Mono.just("Ok")
                .map(ResponseEntity::ok)
                .subscribeOn(Schedulers.elastic());
    }

    @Data
    public static class Hello {
        private String name;
        private String altname;
    }
}
