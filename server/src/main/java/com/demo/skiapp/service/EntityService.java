package com.demo.skiapp.service;

import com.demo.skiapp.entity.AnyEntity;
import com.demo.skiapp.exception.ConflictingResourceException;
import com.demo.skiapp.mapper.CustomDataMapper;
import com.demo.skiapp.model.Entity;
import com.demo.skiapp.repository.AnyEntityRepository;
import java.util.Optional;
import javax.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EntityService {

    private final AnyEntityRepository anyEntityRepository;

    private final CustomDataMapper customDataMapper;

    @Autowired
    public EntityService(AnyEntityRepository anyEntityRepository, CustomDataMapper customDataMapper) {
        this.anyEntityRepository = anyEntityRepository;
        this.customDataMapper = customDataMapper;
    }

    public Entity addEntity(final Entity entity) {
        Optional.ofNullable(entity.getName())
                .map(anyEntityRepository::findById)
                .filter(Optional::isPresent)
                .ifPresent(existing -> {
                    throw new ConflictingResourceException(entity.getName());
                });

        AnyEntity toSave = customDataMapper.toEntityEntity(entity);

        try {
            Thread.sleep(7000);
            log.info("sleeping!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("saving!");
        try {
            return customDataMapper.toEntityModel(anyEntityRepository.save(toSave));
        }
        catch (ConstraintViolationException ex){

            return null;
        }

    }
}
