package com.demo.skiapp.service;


import com.demo.skiapp.entity.PlaceEntity;
import com.demo.skiapp.entity.specification.PlaceSpecification;
import com.demo.skiapp.exception.BadRequestException;
import com.demo.skiapp.exception.ConflictingResourceException;
import com.demo.skiapp.exception.MissingResourceException;
import com.demo.skiapp.mapper.CustomDataMapper;
import com.demo.skiapp.model.AddPlace;
import com.demo.skiapp.model.BookingInfo;
import com.demo.skiapp.model.Place;
import com.demo.skiapp.model.PlaceZone;
import com.demo.skiapp.model.UpdatablePlaceFields;
import com.demo.skiapp.repository.PlaceRepository;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Slf4j
@Service
@Transactional
public class PlaceService {

    private final PlaceRepository placeRepository;

    private final CustomDataMapper customDataMapper;

    @Autowired
    public PlaceService(final PlaceRepository placeRepository, final CustomDataMapper customDataMapper) {
        this.placeRepository = placeRepository;
        this.customDataMapper = customDataMapper;
    }

    public Place bookPlace(final Integer placeId, final BookingInfo bookingInfo) {
        return placeRepository.findById(placeId)
                .map(existing -> {
                    final Integer roomsToBook = bookingInfo.getNumberOfRooms();
                    final Integer currentFreeRooms = existing.getFreeRooms();
                    if (currentFreeRooms < roomsToBook) {
                        throw new BadRequestException("Not enough free rooms!");
                    }
                    existing.setFreeRooms(currentFreeRooms - roomsToBook);
                    return existing;
                })
                .map(customDataMapper::toPlaceModel)
                .orElseThrow(() -> new MissingResourceException(placeId.toString()));
    }

    public Place addNewPlace(final AddPlace newPlace) {
        final String placeName = newPlace.getPlaceName();
        Optional.ofNullable(placeName)
                .map(placeRepository::findByName)
                .filter(Optional::isPresent)
                .ifPresent(existing -> {
                    throw new ConflictingResourceException(placeName);
                });

        PlaceEntity toSave = customDataMapper.toPlaceEntity(newPlace);
        return customDataMapper.toPlaceModel(placeRepository.save(toSave));
    }

    public List<Place> getAllPlaces() {
        return placeRepository.findAll()
                .stream()
                .map(customDataMapper::toPlaceModel)
                .collect(Collectors.toList());
    }

    public Place deletePlace(final Integer placeId) {
        return placeRepository.findById(placeId)
                .map(existing -> {
                    placeRepository.delete(existing);
                    return existing;
                })
                .map(customDataMapper::toPlaceModel)
                .orElseThrow(() -> new MissingResourceException(placeId.toString()));
    }

    public List<PlaceZone> placeZones(final Integer page, final Integer limit) {
        return placeRepository.findCustom(PageRequest.of(page, limit))
                .stream()
                .map(customDataMapper::toPlaceZone)
                .collect(Collectors.toList());
    }


    public Place getPlace(final Integer placeId) {
        return placeRepository.findById(placeId)
                .map(customDataMapper::toPlaceModel)
                .orElseThrow(() -> new MissingResourceException(placeId.toString()));
    }

    public Place update(final Integer placeId, final UpdatablePlaceFields updatablePlaceFields) {
        return placeRepository.findById(placeId)
                .map(existing -> {

                    PlaceEntity newPlace = new PlaceEntity();
                    newPlace.setPlaceId(existing.getPlaceId());
                    newPlace.setPlaceName(updatablePlaceFields.getPlaceName());
                    newPlace.setFreeRooms(updatablePlaceFields.getFreeRooms());
                    newPlace.setZone(updatablePlaceFields.getZone());
                    newPlace.setCountry(updatablePlaceFields.getCountry());

                    return placeRepository.save(newPlace);



//                    existing.setPlaceName(updatablePlaceFields.getPlaceName());
//                    existing.setCountry(updatablePlaceFields.getCountry());
//                    existing.setZone(updatablePlaceFields.getZone());
//                    existing.setFreeRooms(updatablePlaceFields.getFreeRooms());
//
//                    existing.getSports().clear();
//
//                    updatablePlaceFields.getSports()
//                            .forEach(sport -> {
//                                SportEntity newSport = customDataMapper.toSportEntity(sport);
//                                existing.addSport(newSport);
//                            });
//                    return existing;
                })
                .map(customDataMapper::toPlaceModel)
                .orElseThrow(() -> new MissingResourceException(placeId.toString()));
    }

    public List<Place> getPlaces(final String pattern, final Integer page, final Integer limit) {
        return Optional.ofNullable(pattern)
                .filter(StringUtils::isEmpty)
                .map(toSearch -> {
                    Page<PlaceEntity> entity = placeRepository.findAll(new PlaceSpecification(toSearch), PageRequest.of(page, limit));
                    return entity;
                })
                .orElseGet(() -> {
                    Page<PlaceEntity> entity = placeRepository.findAll(PageRequest.of(page, limit));
                    return entity;
                })
                .map(customDataMapper::toPlaceModel)
                .getContent();
    }

    public List<Place> getPlacesByAttributes(final String attributeValue){
//        return placeRepository.findCustomByAttributeFunctionSearch(attributeValue)
//                .stream()
//                .map(customDataMapper::toPlaceModel)
//                .collect(Collectors.toList());
//        List<Integer> result = placeRepository.findComplex(attributeValue, PageRequest.of(0, 1, new Sort(Sort.Direction.DESC,
//                Collections.singleton(""))));

        placeRepository.findById(100);
        placeRepository.findById(100);
        placeRepository.findById(100);
        placeRepository.findById(100);
        placeRepository.findById(100);
        placeRepository.findById(100);
        placeRepository.findById(100);
        placeRepository.findById(100);
        placeRepository.findById(100);

//        placeRepository.findByName(attributeValue);
//        placeRepository.findByName(attributeValue);
//        placeRepository.findByName(attributeValue);
//        placeRepository.findByName(attributeValue);
//        placeRepository.findByName(attributeValue);
//        placeRepository.findByName(attributeValue);
//        placeRepository.findByName(attributeValue);
//        placeRepository.findByName(attributeValue);

//        List<Integer> result = placeRepository.findComplex(attributeValue, PageRequest.of(0, 1));
//        result = placeRepository.findComplex(attributeValue, PageRequest.of(0, 1));
//        result = placeRepository.findComplex(attributeValue, PageRequest.of(0, 1));

        return Collections.emptyList();
    }
}
