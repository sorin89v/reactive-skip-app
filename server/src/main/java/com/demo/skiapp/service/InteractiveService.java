package com.demo.skiapp.service;

import com.demo.skiapp.entity.interactive.Creative;
import com.demo.skiapp.entity.interactive.InteractionActionIntentPk;
import com.demo.skiapp.entity.interactive.InteractiveActionIntent;
import com.demo.skiapp.entity.interactive.InteractiveCreativeDetail;
import com.demo.skiapp.entity.interactive.InteractiveEmitter;
import com.demo.skiapp.entity.interactive.InteractiveIntent;
import com.demo.skiapp.repository.CreativeRepository;
import com.demo.skiapp.repository.InteractiveActionIntentRepository;
import com.demo.skiapp.repository.InteractiveCreativeRepository;
import com.demo.skiapp.repository.InteractiveIntentRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class InteractiveService {

    private final InteractiveCreativeRepository interactiveCreativeRepository;
    private final InteractiveIntentRepository interactiveIntentRepository;
    private final InteractiveActionIntentRepository interactiveActionIntentRepository;
    private final CreativeRepository creativeRepository;

    @Autowired
    public InteractiveService(
            final InteractiveCreativeRepository interactiveCreativeRepository,
            final InteractiveIntentRepository interactiveIntentRepository,
            final InteractiveActionIntentRepository interactiveActionIntentRepository,
            final CreativeRepository creativeRepository) {
        this.interactiveCreativeRepository = interactiveCreativeRepository;
        this.interactiveActionIntentRepository = interactiveActionIntentRepository;
        this.interactiveIntentRepository = interactiveIntentRepository;
        this.creativeRepository = creativeRepository;
    }

    public Creative saveCreative() {
        return creativeRepository.save(Creative.builder()
                .filename("sorinel.mp3")
                .build());
    }

    public InteractiveCreativeDetail saveSomething(final Creative creative) {

        final InteractiveIntent interactiveIntent = InteractiveIntent.builder()
                .method("openUrl")
                .params("www.google.ro")
                //                .actions(new ArrayList<>())
                .build();

        final InteractiveEmitter emitter = InteractiveEmitter.builder()
                .detectionType(InteractiveEmitter.DetectionType.SPEECH)
                .params("emitter Params")
                .build();

        final InteractiveCreativeDetail interactiveCreativeDetail = InteractiveCreativeDetail.builder()
                .message("Do you want to win something")
                .emitters(Collections.singletonList(emitter))
                .actionIntents(new ArrayList<>())
                .build();

        interactiveCreativeDetail.setCreative(creative);

        emitter.setCreativeDetail(interactiveCreativeDetail);

        //        interactiveCreativeDetail.setEmitters(Arrays.asList(emitter));

        final InteractiveCreativeDetail savedCreative = interactiveCreativeRepository.save
                (interactiveCreativeDetail);

        final InteractiveIntent savedIntent = interactiveIntentRepository.save(interactiveIntent);

        final List<String> triggers = Arrays.asList("yes", "of course");

        //        final List<InteractiveActionIntent> actionIntentList = new ArrayList<>();

        triggers.forEach(trigger -> {
            final InteractiveActionIntent interactiveActionIntent = InteractiveActionIntent.builder()
                    .action(savedCreative)
                    .intent(savedIntent)
                    .actionIntentId(InteractionActionIntentPk.builder()
                            .trigger(trigger)
                            .actionId(savedCreative.getId())
                            .intentId(savedIntent.getId())
                            .build())
                    .build();

            //            interactiveIntent.addActionIntent(interactiveActionIntent);
            savedCreative.addActionIntent(interactiveActionIntent);
        });

        return savedCreative;
    }

    public InteractiveCreativeDetail getCreative(final Integer creativeId) {
        //        final InteractiveCreativeDetail actionEmitters =
        //                interactiveCreativeRepository.findActionFetchEmittersByCreativeParent(creativeId)
        //                        .orElse(null);

        final InteractiveCreativeDetail interactiveCreativeDetail =
                interactiveCreativeRepository.findActionFetchIntentsByCreativeParent(creativeId)
                        .orElse(null);

        //        interactiveCreativeDetail.setEmitters(actionEmitters.getEmitters());

        return interactiveCreativeDetail;
    }

    public void deleteCreative(final Creative creative) {
        final Integer creativeId = creative.getId();

        final Optional<InteractiveCreativeDetail> interactiveCreativeDetail =
                interactiveCreativeRepository.findActionFetchIntentsByCreativeParent(creativeId);
        //                        .orElse(null);

        interactiveCreativeDetail.ifPresent(action -> {

            Set<InteractiveIntent> intents = new HashSet<>();

            action.getActionIntents().forEach(interactiveActionIntent -> {

                final InteractiveIntent interactionIntent = interactiveActionIntent.getIntent();
                intents.add(interactionIntent);
            });

            interactiveCreativeRepository.delete(action);
            interactiveIntentRepository.deleteAll(intents);
        });

        //        interactiveIntentRepository.deleteAllByIntentId(intentIds);

        creativeRepository.deleteById(creativeId);
    }

    public void updateCreative(final Creative creative) {
        final Integer creativeId = creative.getId();

        final List<String> triggers = Arrays.asList("new trigger", "another trigger", "one more");

        final String actionMessage = "Here we come with another message!";

        interactiveCreativeRepository.findActionFetchIntentsByCreativeParent(creativeId)
                .map(existingAction -> {

                    final InteractiveEmitter emitter1 = InteractiveEmitter.builder()
                            .detectionType(InteractiveEmitter.DetectionType.SPEECH)
                            .creativeDetail(existingAction)
                            .params("Emitter params1")
                            .build();

                    final InteractiveEmitter emitter2 = InteractiveEmitter.builder()
                            .detectionType(InteractiveEmitter.DetectionType.SPEECH)
                            .creativeDetail(existingAction)
                            .params("Emitter params2")
                            .build();

                    final List<InteractiveEmitter> emitters = Arrays.asList(emitter1, emitter2);

                    final InteractiveIntent interactiveIntent = InteractiveIntent.builder()
                            .method("newMethod")
                            .params("{'url':'newURL'}")
                            .build();

                    final Set<InteractiveIntent> existingIntents = new HashSet<>();

                    existingAction.getActionIntents().forEach(actionIntent ->{
                        existingIntents.add(actionIntent.getIntent());
                    });


                    final InteractiveIntent savedIntent =
                            interactiveIntentRepository.save(interactiveIntent);

                    final List<InteractiveActionIntent> actionsIntents = new ArrayList<>();

                    triggers.forEach(trigger -> {
                        final InteractiveActionIntent actionIntent = InteractiveActionIntent.builder()
                                .intent(savedIntent)
                                .action(existingAction)
                                .actionIntentId(InteractionActionIntentPk.builder()
                                        .trigger(trigger)
                                        .actionId(existingAction.getId())
                                        .intentId(savedIntent.getId())
                                        .build()
                                )
                                .build();

                        actionsIntents.add(actionIntent);
                    });

                    existingAction.getActionIntents().clear();
                    existingAction.getActionIntents().addAll(actionsIntents);

                    existingAction.getEmitters().clear();
                    existingAction.getEmitters().addAll(emitters);

                    existingAction.setMessage(actionMessage);

//                    InteractiveCreativeDetail saved = interactiveCreativeRepository.save(existingAction);

                    interactiveIntentRepository.deleteAll(existingIntents);
                    return existingAction;

//                    return saved;
                })
                .orElseThrow(() -> new IllegalArgumentException("Error"));
    }
}
