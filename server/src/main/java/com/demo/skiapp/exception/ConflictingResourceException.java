package com.demo.skiapp.exception;

public class ConflictingResourceException extends RuntimeException {

    public ConflictingResourceException(String resource) {
        super(String.format("Resource already exists: %s", resource));
    }
}
