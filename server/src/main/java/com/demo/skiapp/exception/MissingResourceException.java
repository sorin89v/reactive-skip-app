package com.demo.skiapp.exception;

public class MissingResourceException extends RuntimeException {

    public MissingResourceException(final String resource) {
        super(String.format("Unable to find mandatory resource: %s", resource));
    }
}
