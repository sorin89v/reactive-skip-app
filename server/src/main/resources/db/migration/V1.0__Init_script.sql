CREATE TABLE places (
    place_id integer auto_increment not null,
    place_name varchar(255) not null unique,
    country varchar(255),
    zone integer not null,
    free_rooms integer not null default 0,
    version bigint not null default 0,
    attributes JSON,
    CHECK (JSON_VALID(attributes)),
    primary key (place_id)
    );
CREATE TABLE sports (
    sport_name varchar(255) not null,
    daily_average_cost numeric(19, 2),
    place_parent integer not null,
    primary key (place_parent, sport_name)
);
-- ALTER TABLE sports ADD CONSTRAINT places_sport_fk FOREIGN KEY (place_parent) REFERENCES places(place_id) ON DELETE CASCADE;
