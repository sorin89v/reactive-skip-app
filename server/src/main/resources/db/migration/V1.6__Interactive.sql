CREATE TABLE `creatives` (
    `id` INT NOT NULL AUTO_INCREMENT COMMENT 'The creative id',
    `filename` VARCHAR(255) NULL COMMENT 'The filename',
    PRIMARY KEY (`id`)
 ) engine = InnoDB default charset = utf8;

CREATE TABLE `creative_actions` (
    `id` INT NOT NULL AUTO_INCREMENT COMMENT 'The call to action id',
    `creative_id` INT NOT NULL COMMENT 'The linked creative id to this action',
    `message` VARCHAR(255) NULL COMMENT 'The message associated to this action',
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_creative_action_id` FOREIGN KEY (`creative_id`) REFERENCES `creatives`(`id`)
 ) engine = InnoDB default charset = utf8;


 CREATE TABLE `creative_emitters` (
    `id` INT NOT NULL AUTO_INCREMENT COMMENT 'The emitter id',
    `creative_action_id` INT NOT NULL  COMMENT 'The associated creative action id',
    `detection_type` ENUM('SHAKE', 'SPEECH') NOT NULL  COMMENT 'The detection type',
    `params` VARCHAR(255) NOT NULL,
     PRIMARY KEY (`id`),
     CONSTRAINT `fk_creative_emitter_action_key` FOREIGN KEY (`creative_action_id`) REFERENCES `creative_actions`(`id`)
) engine = InnoDB default charset = utf8;


CREATE TABLE `creative_intents` (
    `id` INT NOT NULL AUTO_INCREMENT COMMENT 'The creative intent id',
    `method` VARCHAR(255) NOT NULL COMMENT 'The intent method',
    `params` VARCHAR(255) NOT NULL,
     PRIMARY KEY (`id`)
) engine = InnoDB default charset = utf8;


CREATE TABLE `creative_actions_intents` (
    `creative_intent_id` INT NOT NULL COMMENT 'The associated creative intent',
    `creative_action_id` INT NOT NULL COMMENT 'The associated creative action',
    `intent_action_trigger` VARCHAR(255) NOT NULL  COMMENT 'The associated trigger',
     PRIMARY KEY (`creative_intent_id`, `creative_action_id`, `intent_action_trigger`),
     CONSTRAINT `fk_creative_cia_intent_key` FOREIGN KEY (`creative_intent_id`) REFERENCES `creative_intents`(`id`),
     CONSTRAINT `fk_creative_cia_action_key` FOREIGN KEY (`creative_action_id`) REFERENCES `creative_actions`(`id`)
) engine = InnoDB default charset = utf8;
