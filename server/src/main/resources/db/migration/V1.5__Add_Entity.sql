CREATE TABLE entity (
    name varchar(255) not null,
    field varchar(255) not null unique,
    version bigint not null default 0,
    primary key (name)
);
