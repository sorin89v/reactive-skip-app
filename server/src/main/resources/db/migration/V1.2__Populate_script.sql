DELIMITER $$

CREATE PROCEDURE insert_test_data()
BEGIN
  DECLARE i INT DEFAULT 1;

  WHILE i < 10000 DO
    INSERT INTO places (place_id , place_name, country , `zone` , free_rooms, attributes) VALUES (i, CONCAT('place',i), 'ro', 3, 4, '{"type": 111, "name": "any_name", "nested" : [ { "entry": 1}, { "another" : "abc"}, { "nested_more" : "search_free_text_here"}], "param1" : "free_text", "param2" : "another_text", "param3" : 12.35, "param4" : {"inner_nested" : 33, "even_more_nested" : { "most_nested" : "search_also_here"}, "another_nested" : 222}}');
    INSERT INTO sports (sport_name , daily_average_cost, place_parent ) VALUES(CONCAT('sportA', i), 1.25, i);
    INSERT INTO sports (sport_name , daily_average_cost, place_parent ) VALUES(CONCAT('sportB', i), 3.22, i);
    INSERT INTO sports (sport_name , daily_average_cost, place_parent ) VALUES(CONCAT('sportC', i), 5.77, i);
    INSERT INTO agency (agency_id, agency_name, place_agency_id) VALUES (i * 100, CONCAT('agency',i * 100), i);
    SET i = i + 1;
  END WHILE;
END$$

DELIMITER ;

CALL insert_test_data();
DROP PROCEDURE insert_test_data;
